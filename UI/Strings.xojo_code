#tag Module
Protected Module Strings
	#tag Constant, Name = ACCESAUTORISEACTION, Type = String, Dynamic = True, Default = \"Access granted.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Access granted."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Acc\xC3\xA8s autoris\xC3\xA9."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Access granted."
	#tag EndConstant

	#tag Constant, Name = ACTIF, Type = String, Dynamic = True, Default = \"Actif", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Actif"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Actif"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Active"
	#tag EndConstant

	#tag Constant, Name = ADRESSE, Type = String, Dynamic = True, Default = \"Address", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Address"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Adresse"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Address"
	#tag EndConstant

	#tag Constant, Name = AFFICHERINACTIFS, Type = String, Dynamic = True, Default = \"Show inactives", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Show inactives"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Afficher inactifs"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Show inactives"
	#tag EndConstant

	#tag Constant, Name = AJOUTER, Type = String, Dynamic = True, Default = \"Add", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
	#tag EndConstant

	#tag Constant, Name = ANNULER, Type = String, Dynamic = True, Default = \"Annuler", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cancel"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Annuler"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cancel"
	#tag EndConstant

	#tag Constant, Name = BASEPLAN, Type = String, Dynamic = True, Default = \"BASE PLAN", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"BASE PLAN"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"BASE PLAN"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"BASE PLAN"
	#tag EndConstant

	#tag Constant, Name = CODEUTILISATEUR, Type = String, Dynamic = True, Default = \"User code", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User code"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code utilisateur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User code"
	#tag EndConstant

	#tag Constant, Name = COMPTEDESACTIVE, Type = String, Dynamic = True, Default = \"Compte d\xC3\xA9sactiv\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Account disabled"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Compte d\xC3\xA9sactiv\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Account disabled"
	#tag EndConstant

	#tag Constant, Name = CONNEXIONINFO, Type = String, Dynamic = True, Default = \"Connection Infos", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Connection Infos"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Connection Infos"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Informations de connexion"
	#tag EndConstant

	#tag Constant, Name = COURRIELINCONNU, Type = String, Dynamic = True, Default = \"Unknown email", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Unknown email"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Courriel inconnu"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unknown email"
	#tag EndConstant

	#tag Constant, Name = CREATE, Type = String, Dynamic = True, Default = \"Create", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Create"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cr\xC3\xA9er "
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Create"
	#tag EndConstant

	#tag Constant, Name = DASHBOARD, Type = String, Dynamic = True, Default = \"Tableau de bord", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Dashboard"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Tableau de bord"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Dashboard"
	#tag EndConstant

	#tag Constant, Name = DATELABEL, Type = String, Dynamic = True, Default = \"Date :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date :"
	#tag EndConstant

	#tag Constant, Name = DATE_DEBUT, Type = String, Dynamic = True, Default = \"Start date :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de d\xC3\xA9but :"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Start date :"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de d\xC3\xA9but :"
	#tag EndConstant

	#tag Constant, Name = DATE_FIN, Type = String, Dynamic = True, Default = \"Date de fin :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin :"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End date :"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin :"
	#tag EndConstant

	#tag Constant, Name = DELETE, Type = String, Dynamic = True, Default = \"Delete", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Delete"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
	#tag EndConstant

	#tag Constant, Name = DEMARRAGE, Type = String, Dynamic = True, Default = \"Launching...", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Launching..."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9marrage..."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Launching..."
	#tag EndConstant

	#tag Constant, Name = DISCONNECT, Type = String, Dynamic = True, Default = \"L\'application n\'est pas disponible actuellement. S.V.P. r\xC3\xA9essayez plus tard.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The application is not available. Please try again later."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"L\'application n\'est pas disponible actuellement. S.V.P. r\xC3\xA9essayez plus tard."
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The application is not available. Please try again later."
	#tag EndConstant

	#tag Constant, Name = EMAIL, Type = String, Dynamic = True, Default = \"Email", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Email"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Courriel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Email"
	#tag EndConstant

	#tag Constant, Name = EMAILLABEL, Type = String, Dynamic = True, Default = \"Email :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Email :"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Courriel :"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Email :"
	#tag EndConstant

	#tag Constant, Name = ERRCREATENEWUSER, Type = String, Dynamic = True, Default = \"The new user couldn\'t be created.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The new user couldn\'t be created."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Le nouvel utilisateur n\'a pu \xC3\xAAtre cr\xC3\xA9\xC3\xA9."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The new user couldn\'t be created."
	#tag EndConstant

	#tag Constant, Name = ERRDATAENTRY, Type = String, Dynamic = True, Default = \"The information typed is not in the right format. Check if numbers should be used.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The information typed is not in the right format. Check if numbers should be used."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vos informations ne sont pas entr\xC3\xA9es dans le bon format. V\xC3\xA9rifiez si des donn\xC3\xA9es num\xC3\xA9riques sont demand\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The information typed is not in the right format. Check if numbers should be used."
	#tag EndConstant

	#tag Constant, Name = ERRDELETEUSER, Type = String, Dynamic = True, Default = \"The user couldn\'t be deleted.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The user couldn\'t be deleted."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"L\'utilisateur n\'a pas pu \xC3\xAAtre supprim\xC3\xA9."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The user couldn\'t be deleted."
	#tag EndConstant

	#tag Constant, Name = ERREMAILALREADYEXISTS, Type = String, Dynamic = True, Default = \"Email already exists", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Email already exists."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ce courriel existe d\xC3\xA9j\xC3\xA0."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Email already exists."
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur :"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur :"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error :"
	#tag EndConstant

	#tag Constant, Name = ERRLOCK, Type = String, Dynamic = True, Default = \"An error occured while trying to lock records. Please inform your database administrator.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"An error occured while trying to lock records. Please inform your database administrator."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Une erreur s\'est produite lors du verrouillage de l\'enregistrement. S.V.P. contactez l\'administrateur de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"An error occured while trying to lock records. Please inform your database administrator."
	#tag EndConstant

	#tag Constant, Name = ERRREQUIREDDATA, Type = String, Dynamic = True, Default = \"These informations are required.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"These informations are required."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ces informations sont obligatoires."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"These informations are required."
	#tag EndConstant

	#tag Constant, Name = ERRSECURPROFILE, Type = String, Dynamic = True, Default = \"You must select a security profile for the user.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Vous devez choisir le profil de s\xC3\xA9curit\xC3\xA9 de l\'utilisateur."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous devez choisir le profil de s\xC3\xA9curit\xC3\xA9 de l\'utilisateur."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You must select a security profile for the user."
	#tag EndConstant

	#tag Constant, Name = ERRUPDATEUSER, Type = String, Dynamic = True, Default = \"The user couldn\'t be updated.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The user couldn\'t be updated."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Les informations de l\'utilisateur n\'ont pu \xC3\xAAtre mises \xC3\xA0 jour."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The user couldn\'t be updated."
	#tag EndConstant

	#tag Constant, Name = ERRUSERSTATUS, Type = String, Dynamic = True, Default = \"You must select a status for the user.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"You must select a status for the user.\r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous devez choisir le statut de l\'utilisateur."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You must select a status for the user.\r"
	#tag EndConstant

	#tag Constant, Name = FIRSTNAME, Type = String, Dynamic = True, Default = \"First Name", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"First Name"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"First Name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Pr\xC3\xA9nom"
	#tag EndConstant

	#tag Constant, Name = GENERALINFO, Type = String, Dynamic = True, Default = \"General Infos", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"General Infos"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"General Infos"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Informations g\xC3\xA9n\xC3\xA9rales"
	#tag EndConstant

	#tag Constant, Name = HINTFERMERFENETRE, Type = String, Dynamic = True, Default = \"Fermer la fen\xC3\xAAtre", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Fermer la fen\xC3\xAAtre"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Fermer la fen\xC3\xAAtre"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Close window"
	#tag EndConstant

	#tag Constant, Name = INACTIF, Type = String, Dynamic = True, Default = \"Inactif", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inactif"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inactif"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Inactive"
	#tag EndConstant

	#tag Constant, Name = LASTNAME, Type = String, Dynamic = True, Default = \"Last name", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Last name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last name"
	#tag EndConstant

	#tag Constant, Name = MAJ, Type = String, Dynamic = True, Default = \"Mettre \xC3\xA0 jour", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Update"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mettre \xC3\xA0 jour"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Update"
	#tag EndConstant

	#tag Constant, Name = MAJEFFECTUEE, Type = String, Dynamic = True, Default = \"Update was done successfully", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Update was done successfully."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"La mise-\xC3\xA0-jour a \xC3\xA9t\xC3\xA9 effectu\xC3\xA9e."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Update was done successfully."
	#tag EndConstant

	#tag Constant, Name = MEMORISERPARAMETRES, Type = String, Dynamic = True, Default = \"Keep me logged in", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Keep me logged in"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"M\xC3\xA9moriser mes param\xC3\xA8tres"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Keep me logged in"
	#tag EndConstant

	#tag Constant, Name = ML6, Type = String, Dynamic = True, Default = \"\xC2\xA9 Maintenance Logique Syst\xC3\xA8mes - 2016", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC2\xA9 Maintenance Logic Systems - 2016"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC2\xA9 Maintenance Logique Syst\xC3\xA8mes -2016"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"\xC2\xA9 Maintenance Logic Systems - 2016"
	#tag EndConstant

	#tag Constant, Name = MODIFY, Type = String, Dynamic = True, Default = \"Modify", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Modify"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Modifier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Modify"
	#tag EndConstant

	#tag Constant, Name = MOTDEPASSE, Type = String, Dynamic = True, Default = \"Password", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Password"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mot de passe"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Password"
	#tag EndConstant

	#tag Constant, Name = MOTDEPASSECONFIRM, Type = String, Dynamic = True, Default = \"Confirm password", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Confirm password"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Confirmer mot de passe"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Confirm password"
	#tag EndConstant

	#tag Constant, Name = MOTDEPASSEINVALIDE, Type = String, Dynamic = True, Default = \"Mot de passe invalide", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Invalid password"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mot de passe invalide"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Invalid password"
	#tag EndConstant

	#tag Constant, Name = MOTDEPASSELABEL, Type = String, Dynamic = True, Default = \"Mot de passe :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Mot de passe :"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mot de passe :"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Password :"
	#tag EndConstant

	#tag Constant, Name = NIP, Type = String, Dynamic = True, Default = \"Nip", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nip"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nip"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Pin"
	#tag EndConstant

	#tag Constant, Name = NOM, Type = String, Dynamic = True, Default = \"Name", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Name"
	#tag EndConstant

	#tag Constant, Name = NON, Type = String, Dynamic = True, Default = \"No", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Non"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No"
	#tag EndConstant

	#tag Constant, Name = OKUSERDELETED, Type = String, Dynamic = True, Default = \"The user has been permenently deleted.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The user has been permenently deleted."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"L\'utilisateur a \xC3\xA9t\xC3\xA9 effec\xC3\xA9 de fa\xC3\xA7on permanente."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The user has been permenently deleted."
	#tag EndConstant

	#tag Constant, Name = OUI, Type = String, Dynamic = True, Default = \"Yes", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Yes"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Oui"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Yes"
	#tag EndConstant

	#tag Constant, Name = OUVRIRUNESESSION, Type = String, Dynamic = True, Default = \"Ouvrir une session", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Log In"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ouvrir une session"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Log In"
	#tag EndConstant

	#tag Constant, Name = POSTE, Type = String, Dynamic = True, Default = \"Ext.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ext."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Ext."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Poste"
	#tag EndConstant

	#tag Constant, Name = PREFERENCES, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Pr\xC3\xA9f\xC3\xA9rences"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Pr\xC3\xA9f\xC3\xA9rences"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Settings"
	#tag EndConstant

	#tag Constant, Name = PROBLEMECOOKIE, Type = String, Dynamic = False, Default = \"Probl\xC3\xA8me d\'enregistrement de Cookie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Probl\xC3\xA8me d\'enregistrement de Cookie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Probl\xC3\xA8me d\'enregistrement de Cookie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cookie recording problem"
	#tag EndConstant

	#tag Constant, Name = PROFILACCES, Type = String, Dynamic = True, Default = \"Security Profile", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Security Profile"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Profil de s\xC3\xA9curit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Security Profile"
	#tag EndConstant

	#tag Constant, Name = QUITTER, Type = String, Dynamic = True, Default = \"Quit", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Quit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Quitter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Quit"
	#tag EndConstant

	#tag Constant, Name = REQUIS, Type = String, Dynamic = True, Default = \"Required", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Requis"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required"
	#tag EndConstant

	#tag Constant, Name = RETOURAULOGIN, Type = String, Dynamic = True, Default = \"Back to login", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Back to login"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Retour"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Back to login"
	#tag EndConstant

	#tag Constant, Name = ROWLOCKED, Type = String, Dynamic = True, Default = \"Data Locked by ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Data Locked by "
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Data Locked by "
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Donn\xC3\xA9es verrouill\xC3\xA9es par "
	#tag EndConstant

	#tag Constant, Name = SITEWEB, Type = String, Dynamic = True, Default = \"Web site", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Web site"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Site web"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Web site"
	#tag EndConstant

	#tag Constant, Name = SORTIE, Type = String, Dynamic = True, Default = \"sortie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sortie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sortie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Logout"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Status", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Status"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = STOREML6, Type = String, Dynamic = True, Default = \"Store ML6", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Store ML6"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Boutique ML6"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Store ML6"
	#tag EndConstant

	#tag Constant, Name = SUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
	#tag EndConstant

	#tag Constant, Name = TECHEOLOEM, Type = String, Dynamic = True, Default = \"Applications", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Applications"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Applications"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Applications"
	#tag EndConstant

	#tag Constant, Name = TELECHARGEMENTFORM, Type = String, Dynamic = True, Default = \"Download mobile form", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Download mobile form"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"T\xC3\xA9l\xC3\xA9charger le formulaire mobile"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Download mobile form"
	#tag EndConstant

	#tag Constant, Name = TELEPHONE, Type = String, Dynamic = True, Default = \"Phone", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Phone"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Phone"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"T\xC3\xA9l\xC3\xA9phone"
	#tag EndConstant

	#tag Constant, Name = TELEPHONEM, Type = String, Dynamic = True, Default = \"Mobile", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Mobile"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Mobile"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cellulaire"
	#tag EndConstant

	#tag Constant, Name = TITLEMODAL, Type = String, Dynamic = True, Default = \"TECHEOL GOP Application", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"TECHEOL GOP Application"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"TECHEOL GOP Application"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Application TECHEOL GOP"
	#tag EndConstant

	#tag Constant, Name = TITRE, Type = String, Dynamic = True, Default = \"Title", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Title"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Titre"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Title"
	#tag EndConstant

	#tag Constant, Name = TYPELABEL, Type = String, Dynamic = True, Default = \"Type :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Type :"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Type :"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Type :"
	#tag EndConstant

	#tag Constant, Name = UPDATEMODAL, Type = String, Dynamic = True, Default = \"Donn\xC3\xA9es mises \xC3\xA0 jour.", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Abonnement r\xC3\xA9ussi!"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Subscription successfull"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Abonnement r\xC3\xA9ussi!"
	#tag EndConstant

	#tag Constant, Name = USERID, Type = String, Dynamic = True, Default = \"User ID", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User ID"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom d\'utilisateur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User ID"
	#tag EndConstant

	#tag Constant, Name = USERMANAGEMENT, Type = String, Dynamic = True, Default = \"User Management", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User Management"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Gestion des utilisateurs"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User Management"
	#tag EndConstant

	#tag Constant, Name = UTILISATEURINCONNU, Type = String, Dynamic = True, Default = \"Unknown user", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Unknown user"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateur inconnu"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unknown user"
	#tag EndConstant

	#tag Constant, Name = UTILISATEURS, Type = String, Dynamic = True, Default = \"Users", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Users"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateurs"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Users"
	#tag EndConstant

	#tag Constant, Name = VALIDER, Type = String, Dynamic = True, Default = \"Validate", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Validate"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Valider"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Validate"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
