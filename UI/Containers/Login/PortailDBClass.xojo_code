#tag Class
Protected Class PortailDBClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  Select Case Session.environnementProp
		  Case Is = "Test"
		    Self.portail_ldap_host = Self.LDAPTEST
		    Self.portail_ldap_domain = Self.LDAPTESTDOMAIN
		    Self.portail_ldap_user = Self.LDAPTESTUSER
		    Self.portail_ldap_user_password = Self.LDAPTESTUSERPASSWORD
		  Case Is = "Preproduction"
		    Self.portail_ldap_host = Self.LDAPPREPRODUCTION
		    Self.portail_ldap_domain = Self.LDAPPREPRODDOMAIN
		    Self.portail_ldap_user = Self.LDAPPREPRODUSER
		    Self.portail_ldap_user_password = Self.LDAPPREPRODUSERPASSWORD
		  Case Else
		    Self.portail_ldap_host = Self.LDAPPROD
		    Self.portail_ldap_domain = Self.LDAPPRODDOMAIN
		    Self.portail_ldap_user = Self.LDAPPRODUSER
		    Self.portail_ldap_user_password = Self.LDAPPRODUSERPASSWORD
		  End Select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function user_AD_Validation(pUserName As String, pPassword As String) As String
		  // Vérification si l'utilisateur fait partie des utilisateurs de l'Active Directory
		  Dim message As String = "Succes"
		  Dim strQuery As String
		  
		  Dim logonServer as String = Self.portail_ldap_host
		  Dim adoConnection, adoCommand,adoRecordset As OleObject
		  
		  strQuery = "SELECT sAMAccountName, CN, userAccountControl FROM 'LDAP://" + logonServer + "' WHERE objectCategory = 'Person' "
		  
		  adoConnection = new OLEOBJECT("ADODB.Connection")
		  adoConnection.Provider = "ADsDSOOBJECT"
		  adoConnection.Properties("Encrypt Password").Value = False
		  adoConnection.open ("DS Query",Self.portail_ldap_domain + Self.portail_ldap_user, Self.portail_ldap_user_password)
		  
		  adoCommand = New OleObject("ADODB.Command")
		  adoCommand.ActiveConnection = adoConnection
		  adoCommand.CommandText = strQuery
		  //adoCommand.Properties("Size Limit") = 1
		  Dim strUser As String =  adoConnection.Properties("User ID").Value
		  Dim strPassword As String = adoConnection.Properties("Password").Value 
		  
		  adoRecordset = New OLEObject("ADODB.Recordset")
		  try
		    adoRecordset = adoCommand.Execute()
		  Catch ExecuteError As OLEException
		    message = ExecuteError.Message + " Lecture de tous les utilisateurs"
		    adoConnection.close
		    adoConnection = Nil
		    adoCommand = Nil
		    adoRecordset = Nil
		    Return message
		  End Try
		  
		  adoRecordset.MoveFirst
		  Dim index As Integer = 0
		  Dim actifInactif As String = ""
		  Dim portail_user_list_trav as New Dictionary
		  Dim trouve As Boolean = False
		  While Not adoRecordset.eof
		    Dim sAMAccountName As String = adoRecordset.Fields("sAMAccountName").Value
		    Dim commonName As String = adoRecordset.Fields("CN").Value
		    Dim userAccountControl As String = adoRecordset.Fields("userAccountControl").Value
		    If sAMAccountName = pUserName Then 
		      trouve = True
		      actifInactif = userAccountControl
		    End If
		    If userAccountControl <> "514" Then // Éviter les comptes désactivés
		      portail_user_list_trav.Value(sAMAccountName) = commonName
		      index = index + 1
		    End If
		    adoRecordset.MoveNext
		  Wend
		  //portail_user_list = portail_user_list_trav
		  portail_user_list_trav = Nil
		  adoConnection.close
		  adoConnection = Nil
		  adoCommand = Nil
		  adoRecordset = Nil
		  
		  If trouve = False Then 
		    message = LoginModule.UTILISATEURINCONNUAD
		    //portail_Login_Container.portail_userCodeField.Style = labelTextLeft12ERR
		    Return message
		  End If
		  
		  // Compte désactivé
		  If actifInactif = "514" Then 
		    message = LoginModule.COMPTEDESACTIVEAD
		    //portail_Login_Container.portail_userCodeField.Style = labelTextLeft12ERR
		    Return message
		  End If
		  
		  // Vérification si le mot de passe est valide
		  adoConnection = new OLEOBJECT("ADODB.Connection")
		  adoConnection.Provider = "ADsDSOOBJECT"
		  adoConnection.Properties("Encrypt Password") = False
		  adoConnection.open ("DS Query", Self.portail_ldap_domain + pUserName, pPassword)
		  
		  adoCommand = New OleObject("ADODB.Command")
		  adoCommand.ActiveConnection = adoConnection
		  adoCommand.CommandText = strQuery
		  //adoCommand.Properties("Size Limit") = 1
		  strUser =  adoConnection.Properties("User ID").Value
		  strPassword = adoConnection.Properties("Password").Value 
		  
		  adoRecordset = New OLEObject("ADODB.Recordset")
		  try
		    adoRecordset = adoCommand.Execute()
		  Catch ExecuteError As OLEException
		    message = LoginModule.USERPASSINVALIDAD
		    Return message
		  End Try
		  
		  adoConnection.close
		  adoConnection = Nil
		  adoCommand = Nil
		  adoRecordset = Nil
		  
		  Return message
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		portail_ldap_domain As String
	#tag EndProperty

	#tag Property, Flags = &h0
		portail_ldap_host As String
	#tag EndProperty

	#tag Property, Flags = &h0
		portail_ldap_user As String
	#tag EndProperty

	#tag Property, Flags = &h0
		portail_ldap_user_password As String
	#tag EndProperty


	#tag Constant, Name = LDAPPREPRODDOMAIN, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"rpf\\"
	#tag EndConstant

	#tag Constant, Name = LDAPPREPRODUCTION, Type = String, Dynamic = True, Default = \"RPF-DC1.rpf.ca", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = LDAPPREPRODUSER, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"TechML6"
	#tag EndConstant

	#tag Constant, Name = LDAPPREPRODUSERPASSWORD, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ML62016!"
	#tag EndConstant

	#tag Constant, Name = LDAPPROD, Type = String, Dynamic = True, Default = \"RPF-DC1.rpf.ca", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = LDAPPRODDOMAIN, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"rpf\\"
	#tag EndConstant

	#tag Constant, Name = LDAPPRODUSER, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"TechML6"
	#tag EndConstant

	#tag Constant, Name = LDAPPRODUSERPASSWORD, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ML62016!"
	#tag EndConstant

	#tag Constant, Name = LDAPTEST, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"192.168.0.243"
	#tag EndConstant

	#tag Constant, Name = LDAPTESTDOMAIN, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ml6g\\"
	#tag EndConstant

	#tag Constant, Name = LDAPTESTUSER, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"administrateur"
	#tag EndConstant

	#tag Constant, Name = LDAPTESTUSERPASSWORD, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"@Ml612345@"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="portail_ldap_domain"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="portail_ldap_host"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="portail_ldap_user"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="portail_ldap_user_password"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
