#tag WebPage
Begin WebContainer MobileLogin
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   440
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "1816279039"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   320
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle WR1
      Cursor          =   0
      Enabled         =   True
      Height          =   314
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Scope           =   0
      Style           =   "938258431"
      TabOrder        =   -1
      Top             =   119
      VerticalCenter  =   0
      Visible         =   True
      Width           =   302
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox MemoCheckbox
      Caption         =   "#LoginModule.MEMORISERPARAMETRES"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   87
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Scope           =   0
      Style           =   "1001844735"
      TabOrder        =   3
      Top             =   354
      Value           =   True
      VerticalCenter  =   0
      Visible         =   True
      Width           =   198
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField portail_userCodeField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   "#LoginModule.CODEUTILISATEUR"
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   "#LoginModule.CODEUTILISATEUR"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   ""
      TextAlign       =   0
      Top             =   245
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ConnectStatusLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   False
      Height          =   39
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   87
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   True
      Scope           =   0
      Style           =   "845094911"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   383
      VerticalCenter  =   0
      Visible         =   True
      Width           =   198
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ImageViewDeskTopLogin
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   100
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   110
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   947681279
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   133
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel WLStatut
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   26
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   33
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "1001844735"
      TabOrder        =   7
      Text            =   "#LoginModule.STATUT"
      TextAlign       =   0
      Top             =   354
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVStatut
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   825221119
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   383
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVTitle
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   100
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   1809229823
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   298
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField PWField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   "#LoginModule.MOTDEPASSE"
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   "#LoginModule.MOTDEPASSE"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   280
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel PreprodOuProdLabel
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   11
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "1529059327"
      TabOrder        =   1
      Text            =   "Preproduction"
      TextAlign       =   0
      Top             =   153
      VerticalCenter  =   0
      Visible         =   False
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ProdOuTestLabel
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   11
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "1529059327"
      TabOrder        =   1
      Text            =   "Test"
      TextAlign       =   0
      Top             =   153
      VerticalCenter  =   0
      Visible         =   False
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel VersionLabel
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   18
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   11
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "1529059327"
      TabOrder        =   1
      Text            =   "V13"
      TextAlign       =   0
      Top             =   133
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton LoginButton
      AutoDisable     =   False
      Caption         =   "#LoginModule.LOGIN"
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   75
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Scope           =   2
      Style           =   "447817727"
      TabOrder        =   8
      Top             =   315
      VerticalCenter  =   0
      Visible         =   True
      Width           =   171
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  ouverture
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  majLoginDetail
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub appelTecheolGOP(tableauCie_param As String)
		  // Mettre le user dans une variable custom Postgresql
		  //Dim strSQL As String = "SET application.currentuser TO '" + portail_userCodeField.Text + "';"
		  //Session.bdGroupeRPF.SQLExecute(strSQL)
		  
		  //Mettre à jour  le cookie
		  If MemoCheckbox.Value Then
		    Session.Cookies.Set(Session.COOKIEUSERMEMO, portail_userCodeField.Text)
		    #if DebugBuild Then
		      Session.Cookies.Set(Session.COOKIEUSERMEMO, portail_userCodeField.Text)
		    #EndIf
		  Else
		    Session.Cookies.Remove(Session.COOKIEUSERMEMO)
		  End If
		  
		  // Création des cookies
		  If Session.environnementProp = "Preproduction" Then
		    Session.Cookies.Set(Session.COOKIEUSER, portail_userCodeField.Text)
		    Session.Cookies.Set(Session.COOKIEOEMSECURITYPROFILE, Session.sUser.user_role)
		    Session.Cookies.Set(Session.COOKIEUSERCie, tableauCie_param)
		  ElseIf Session.environnementProp = "Test" Then
		    Session.Cookies.Set(Session.COOKIEUSER, portail_userCodeField.Text)
		    Session.Cookies.Set(Session.COOKIEOEMSECURITYPROFILE, Session.sUser.user_role)
		    Session.Cookies.Set(Session.COOKIEUSERCie, tableauCie_param)
		  Else
		    //Production
		    Session.Cookies.Set(Session.COOKIEUSER, portail_userCodeField.Text)
		    Session.Cookies.Set(Session.COOKIEOEMSECURITYPROFILE, Session.sUser.user_role)
		    Session.Cookies.Set(Session.COOKIEUSERCie, tableauCie_param)
		  End If
		  
		  Dim str As String = Session.sUser.user_code
		  //Mode débug
		  #if DebugBuild Then
		    Session.Cookies.Set(Session.COOKIEUSER, portail_userCodeField.Text)
		    Session.Cookies.Set(Session.COOKIEOEMSECURITYPROFILE, Session.sUser.user_role)
		    Session.Cookies.Set(Session.COOKIEUSERCie, tableauCie_param)
		  #Endif
		  
		  
		  Select Case Session.environnementProp
		  Case Is = "Test"
		    showURL("http://192.168.0.51:7071")
		  Case Is = "Preproduction"
		    ShowURL("http://techeolpreprod.ml6.tech:7071")
		  Case Else
		    ShowURL("http://www.ml6techeol.tech:7071")
		  End Select
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub majLoginDetail()
		  If Self.portail_userCodeField.Text <> "" Then
		    PWField.Text = ""
		    PWField.SetFocus
		    IVStatut.Picture = Deconnecte40x40
		  Else
		    Self.portail_userCodeField.SetFocus
		  End If
		  
		  resetFieldsStyle
		  
		  //Label hors production
		  PreprodOuProdLabel.Visible = False
		  ProdOuTestLabel.Visible = False
		  
		  Select Case Session.environnementProp
		  Case Is = "Test"  // Nous sommes en test
		    ProdOutestLabel.Visible = True
		  Case Is = "Preproduction"
		    PreprodOuProdLabel.Visible = True
		  End Select
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ouverture()
		  
		  If Session.Cookies.Value(Session.COOKIEUSERMEMO) <> "" Then
		    Self.portail_userCodeField.Text = Session.Cookies.Value(Session.COOKIEUSERMEMO)
		    Self.MemoCheckbox.Value = True
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub resetFields()
		  
		  ConnectStatusLabel.Text = ""
		  
		  portail_userCodeField.Text = ""
		  PWField.Text = ""
		  
		  portail_userCodeField.Style = Nil
		  PWField.Style = Nil
		  
		  IVStatut.Picture = Deconnecte40x40
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub resetFieldsStyle()
		  
		  ConnectStatusLabel.Text = ""
		  
		  portail_userCodeField.Style = Nil
		  PWField.Style = Nil
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub traiteAction()
		  Dim message As String = ""
		  
		  //Déceler l'absence des paramètres d'ouverture de session
		  If (portail_userCodeField.Text = "" Or PWField.Text = "") Then
		    portail_userCodeField.Style = TextFieldErrorStyle
		    PWField.Style = TextFieldErrorStyle
		    ConnectStatusLabel.Text = LoginModule.USERPASSWORDCOMPULSORY
		    Exit Sub
		  End If
		  
		  // Mettre en Lowercase
		  portail_userCodeField.Text = Lowercase(portail_userCodeField.Text)
		  
		  If Session.utilisationActiveDirectory Then
		    Dim sPortailDB As PortailDBClass = New PortailDBClass
		    message = sPortailDB.user_AD_Validation(portail_userCodeField.Text, PWField.Text)
		    sPortailDB  = Nil
		    If message <> "Succes" Then
		      portail_userCodeField.Style = TextFieldErrorStyle
		      ConnectStatusLabel.Text = message
		      PWField.Style = TextFieldErrorStyle
		      Exit Sub
		    End If
		  End If
		  
		  // Vérification si l'utilisateur existe dans les base de données
		  Dim tableauCie As String = ""
		  
		  // HL Thériault
		  message = GenControlModule.verifBD("hlther",portail_userCodeField.Text, PWField.Text)
		  If message <> "Succes" And message <> "Utilisateur pas dans la bd" Then
		    ConnectStatusLabel.Text = message
		    Exit Sub
		  End If 
		  If message <> "Utilisateur pas dans la bd" Then
		    tableauCie = tableauCie + "hlther,"
		  End If 
		  
		  // RPF
		  message = GenControlModule.verifBD("rpf",portail_userCodeField.Text, PWField.Text)
		  If message <> "Succes" And message <> "Utilisateur pas dans la bd" Then
		    ConnectStatusLabel.Text = message
		    Exit Sub
		  End If 
		  If message <> "Utilisateur pas dans la bd" Then
		    tableauCie = tableauCie + "rpf,"
		  End If 
		  
		  // TechÉol
		  message = GenControlModule.verifBD("techeol",portail_userCodeField.Text, PWField.Text)
		  If message <> "Succes" And message <> "Utilisateur pas dans la bd" Then
		    ConnectStatusLabel.Text = message
		    Exit Sub
		  End If 
		  If message <> "Utilisateur pas dans la bd" Then
		    tableauCie = tableauCie + "techeol,"
		  End If 
		  
		  // Enlever la virgule à la fin
		  tableauCie = Left(tableauCie, Len(tableauCie)-1)
		  
		  // Appeler le menu de gestion des applications
		  appelTecheolGOP(tableauCie)
		  
		  
		  //Dim sParametre As ParametreClass = New ParametreClass()
		  //Dim parametreRS As RecordSet
		  //parametreRS = sParametre.loadDataByFieldCond(Session.bdGroupeRPF, "parametre", "parametre_nom", " = ", "activedirectory", " And ","parametre_tri", " = ", "1", "parametre_tri")
		  //If parametreRS <> Nil Then
		  //If parametreRS.RecordCount = 1 Then
		  //Session.utilisationActiveDirectory = True
		  //End If
		  //End If
		  //sParametre = Nil
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		appsShown As Boolean = False
	#tag EndProperty


#tag EndWindowCode

#tag Events MemoCheckbox
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteAction
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LoginButton
	#tag Event
		Sub Action()
		  traiteAction
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="appsShown"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
