#tag Class
Protected Class ParametreClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Function chargementTitreTable(parametreDB As PostgreSQLDatabase, table_name As String, prefix As String , table_zone As String) As String
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  Dim valeurTitre As String = ""
		  
		  strSQL = "SELECT * FROM " + table_name + " WHERE " + prefix + "_nom = '" + table_zone  +"' " + _
		  "AND " + prefix + "_tri = 0  "
		  recordSet =  parametreDB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    Dim lang as string = Session.Header("Accept-Language")
		    If instr(lang, "fr")>0 Then
		      valeurTitre = recordSet.Field( prefix + "_desc_fr").StringValue
		    Else
		      valeurTitre = recordSet.Field(prefix + "_desc_en").StringValue
		    End If
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  
		  Return valeurTitre
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdGroupeRPF, "parametre", "parametre")
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		parametre_cle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_cleetr_cle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_cleetr_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_desc_en As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_desc_fr As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_statut As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_tri As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_cle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_cleetr_cle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_cleetr_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_desc_en"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_desc_fr"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_statut"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_tri"
			Group="Behavior"
			Type="Integer"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
