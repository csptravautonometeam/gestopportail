#tag Class
Protected Class UserClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Function userValidation(userName_param As String, password_param As String) As String
		  Dim message As String = "Succes"
		  
		  //Chercher le nom d'utilisateur dans la base de données
		  Dim userRS As RecordSet = Session.sUser.loadDataByField(Session.bdGroupeRPF, "portail_user", "user_code", userName_param, "user_code")
		  If userRS.RecordCount = 0 Then
		    message = LoginModule.UTILISATEURINCONNUBD
		    Return message
		  End If
		  Session.sUser.LireDonneesBD(userRS, "user")
		  userRS.Close
		  userRS = Nil
		  
		  // Vérification du mot de passe
		  // Le mot de passe a été vérifié auparavant pour un utilisateur de l'Active Directory
		  If Not Session.utilisationActiveDirectory Then
		    If Session.sUser.user_password <> password_param Then
		      message = LoginModule.USERPASSINVALIDDB
		      Return message
		    End If
		  End If
		  
		  Return "Succes"
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		user_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_email As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_firstname As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_lastname As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_password As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_poste As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_role As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_status As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_telephone As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_tel_mobile As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_email"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_firstname"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_lastname"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_password"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_poste"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_role"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_status"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_telephone"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_tel_mobile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
