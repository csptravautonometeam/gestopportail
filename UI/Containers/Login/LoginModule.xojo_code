#tag Module
Protected Module LoginModule
	#tag Constant, Name = ACCESAUTORISEACTION, Type = String, Dynamic = True, Default = \"Access granted.", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Access granted."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Acc\xC3\xA8s autoris\xC3\xA9."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Access granted."
	#tag EndConstant

	#tag Constant, Name = CODEUTILISATEUR, Type = String, Dynamic = True, Default = \"User code", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User code"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code utilisateur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User code"
	#tag EndConstant

	#tag Constant, Name = COMPTEDESACTIVEAD, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Account disabled in Active Directory"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Compte d\xC3\xA9sactiv\xC3\xA9 dans l\'Active Directory"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Account disabled in Active Directory"
	#tag EndConstant

	#tag Constant, Name = LOGIN, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Login"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Se connecter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Login"
	#tag EndConstant

	#tag Constant, Name = MEMORISERPARAMETRES, Type = String, Dynamic = True, Default = \"Keep me logged in", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Keep me logged in"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"M\xC3\xA9moriser mes param\xC3\xA8tres"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Keep me logged in"
	#tag EndConstant

	#tag Constant, Name = ML6, Type = String, Dynamic = True, Default = \"\xC2\xA9 Maintenance Logique Syst\xC3\xA8mes - 2017", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC2\xA9 Maintenance Logic Systems - 2017"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC2\xA9 Maintenance Logique Syst\xC3\xA8mes -2017"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"\xC2\xA9 Maintenance Logic Systems - 2017"
	#tag EndConstant

	#tag Constant, Name = MOTDEPASSE, Type = String, Dynamic = True, Default = \"Password", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Password"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mot de passe"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Password"
	#tag EndConstant

	#tag Constant, Name = PROBLEMECOOKIE, Type = String, Dynamic = False, Default = \"Probl\xC3\xA8me d\'enregistrement de Cookie", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Probl\xC3\xA8me d\'enregistrement de Cookie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Probl\xC3\xA8me d\'enregistrement de Cookie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cookie recording problem"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Status", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Status"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = USERNOAPPLICATION, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No application assigned to this user"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucune application assign\xC3\xA9e \xC3\xA0 cet utilisateur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No application assigned to this user"
	#tag EndConstant

	#tag Constant, Name = USERPASSINVALIDAD, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User/Password invalid in Active Directory"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateur/Mot de passe invalide dans l\'Active Directory"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User/Password invalid in Active Director"
	#tag EndConstant

	#tag Constant, Name = USERPASSINVALIDDB, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User/Password invalid in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateur/Mot de passe invalide dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User/Password invalid in the database"
	#tag EndConstant

	#tag Constant, Name = USERPASSWORDCOMPULSORY, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User and password is compulsory"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateur et mot de passe obligatoire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User and password is compulsory"
	#tag EndConstant

	#tag Constant, Name = UTILISATEURINCONNUAD, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Unknown user in Active Directory"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateur inconnu dans l\'Active Directory"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unknown user in Active Directory"
	#tag EndConstant

	#tag Constant, Name = UTILISATEURINCONNUBD, Type = String, Dynamic = True, Default = \"", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Unknown user in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateur inconnu dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unknown user in the database"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
