#tag Module
Protected Module GenControlModule
	#tag Method, Flags = &h0
		Function verifBD(nomBD_param As String, portail_userCodeField_param As String, pwField_param As String) As String
		  Dim message As String = "Succes"
		  
		  Session.centralDatabaseName = nomBD_param
		  If Session.environnementProp = "Test" Then  // Nous sommes en test
		    //Session.centralDatabaseName = Globals.CENTRALDATABASENAMETEST
		  End If
		  
		  Session.bdGroupeRPF = New PostgreSQLDatabase
		  Session.bdGroupeRPF.Host = Session.host
		  Session.bdGroupeRPF.UserName = GenControlModule.USER
		  Session.bdGroupeRPF.Password = GenControlModule.PASSWORD
		  Session.bdGroupeRPF.DatabaseName =Session.centralDatabaseName
		  
		  If Not (Session.bdGroupeRPF.Connect) Then
		    message = GMTextConversion(Session.bdGroupeRPF.ErrorMessage)
		    GoTo Fin
		  Else
		    Session.bdGroupeRPF.SQLExecute("SET search_path TO form, equip, dbglobal, portail, locking, audit, public;")
		    If  Session.bdGroupeRPF .Error Then
		      message = GMTextConversion(Session.bdGroupeRPF.ErrorMessage)
		      GoTo Fin
		    End If
		  End If
		  
		  // Création de l'objet user
		  Session.sUser = Nil
		  Session.sUser = New UserClass(Session.bdGroupeRPF, "portail_user", "user")
		  
		  // Vérification de l'utilisateur dans la base de données 
		  message = Session.sUser.userValidation(Lowercase(portail_userCodeField_param),pwField_param)
		  If message <> "Succes" Then
		    message = "Utilisateur pas dans la bd"
		  End If
		  
		  Fin:
		  Session.bdGroupeRPF.Close
		  Session.bdGroupeRPF = Nil
		  
		  Return message
		End Function
	#tag EndMethod


	#tag Constant, Name = CARRIAGERETURN, Type = String, Dynamic = True, Default = \"\r", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"techeol"
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMETEST, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"techeol"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOPREPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"preprod.ml6techeol.tech"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"www.ml6techeol.tech"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOTEST, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"192.168.0.51"
	#tag EndConstant

	#tag Constant, Name = HOSTPREPRODUCTION, Type = String, Dynamic = True, Default = \"preprod.ml6techeol.tech", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"localhost"
	#tag EndConstant

	#tag Constant, Name = HOSTTEST, Type = String, Dynamic = True, Default = \"192.168.0.51", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LEAVEAPPMESSAGE, Type = String, Dynamic = True, Default = \"Vous \xC3\xAAtes en train de quitter cette application.\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You are about to leave this application."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
	#tag EndConstant

	#tag Constant, Name = PASSWORD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"postgres"
	#tag EndConstant

	#tag Constant, Name = PHOTOFOLDER, Type = String, Dynamic = True, Default = \"baseplan/reppale", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"baseplan/reppale"
	#tag EndConstant

	#tag Constant, Name = USER, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"admin"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
