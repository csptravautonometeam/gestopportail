#tag Class
Protected Class GenWebListBox
Inherits WebListBox
	#tag Event
		Sub HeaderPressed(Column as Integer)
		  SortListBox(column)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Me.HeaderStyle = labeLTextLeft14
		  Me.Style= labeLTextLeft13
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub changeCellStyle(r As Integer, c As Integer, removeStyle As Boolean)
		  If me.RowCount > 0 Then
		    If Not removeStyle Then
		      me.CellStyle(r, c) = labelTextLeft12Inactif
		    Else
		      me.CellStyle(r, c) = labelTextLeft12
		    End If
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SortListBox(sortColumn As Integer)
		  Dim memoryDB As New SQLiteDatabase
		  Dim rec As New DatabaseRecord
		  Dim rs As RecordSet
		  Dim numericalColumn As Boolean = True
		  Dim tempSQL As String
		  Dim tempString As String
		  Dim sortAscending As Boolean = True
		  
		  If Me = nil Then Return
		  
		  If Me.ColumnCount < 1 Then Return 'no point sorting when no columns!
		  If Me.RowCount <= 1 Then Return 'no point sorting when no rows, or only one row!
		  If not memoryDB.Connect Then Return 'can't access the database
		  
		  'check If the column is already sorted by going down the column until we find two in descending order, otherwise exit And keep as ascending order. This fixes the issue when lots of values are the same.
		  For tempInt As Integer = 1 to Me.RowCount - 1
		    Dim b1 As Boolean
		    If Me.Cell(tempInt - 1, sortColumn).val = 0 Then b1 = True
		    //dim s1 As string =  Uppercase(ReplaceAll(ReplaceAll(ReplaceAll(Me.Cell(tempInt - 1, sortColumn), ",", ""), " ", ""), "-", ""))
		    //dim s2 as String =Uppercase(ReplaceAll(ReplaceAll(ReplaceAll(Me.Cell(tempInt, sortColumn), ",", ""), " ", ""), "-", ""))
		    dim s1 As string =  Me.Cell(tempInt - 1, sortColumn)
		    dim s2 as String =Me.Cell(tempInt, sortColumn)
		    Dim b2 as Boolean
		    If s1 < s2 then b1 = True
		    Dim s3 As Double = ReplaceAll(ReplaceAll(ReplaceAll(Me.Cell(tempInt - 1, sortColumn), ",", ""), " ", ""), "-", "").val
		    Dim s4 As Double = ReplaceAll(ReplaceAll(ReplaceAll(Me.Cell(tempInt, sortColumn), ",", ""), " ", ""), "-", "").val
		    Dim b3 As Boolean
		    If s3 < s4 Then b2 = true
		    If (Me.Cell(tempInt - 1, sortColumn).val = 0 And Uppercase(Me.Cell(tempInt - 1, sortColumn)) < Uppercase(Me.Cell(tempInt, sortColumn))) Or ReplaceAll(ReplaceAll(ReplaceAll(Me.Cell(tempInt - 1, sortColumn), ",", ""), " ", ""), "-", "").val < ReplaceAll(ReplaceAll(ReplaceAll(Me.Cell(tempInt, sortColumn), ",", ""), " ", ""), "-", "").val Then
		      sortAscending = False
		      Exit For
		    End If
		  Next
		  
		  'build an in-memory SQLite database to hold the column data
		  tempSQL = "CREATE TABLE sortTable ("
		  For tempInt As Integer = 0 to Me.ColumnCount - 1
		    tempSQL = tempSQL + "myColumn" + str(tempInt) + " TEXT COLLATE NOCASE, "
		  Next
		  tempSQL = tempSQL + "extraSortColumnWAD DOUBLE"
		  tempSQL = tempSQL + ")"
		  memoryDB.SQLExecute(tempSQL)
		  If memoryDB.Error Then Return 'not sure why we got an error, but leave the table alone
		  
		  'place the WebListBox data into the database
		  For tempInt As Integer = 0 to Me.RowCount - 1
		    rec = New DatabaseRecord
		    For tempInt2 As Integer = 0 to Me.ColumnCount - 1
		      rec.Column("myColumn" + str(tempInt2)) = Me.Cell(tempInt, tempInt2)
		      'check If the chosen column is full of only numerical data
		      If sortColumn = tempInt2 And numericalColumn Then
		        tempString = Uppercase(Me.Cell(tempInt, tempInt2))
		        tempString = ReplaceAll(tempString, ",", "") 'remove commas
		        tempString = ReplaceAll(tempString, " ", "") 'remove spaces
		        If tempString <> "" And tempString <> str(tempString.val) And tempString.val < 1000000  Then
		          numericalColumn = False 'check If what's left over is a number
		        End If
		      End If
		    Next
		    memoryDB.InsertRecord("sortTable", rec)
		    If memoryDB.Error Then Return 'not sure why we got an error, but leave the table alone
		  Next
		  
		  'extract the sorted table from the database, wipe the table Then return the data
		  tempSQL = "SELECT * FROM sortTable "
		  If numericalColumn Then
		    memoryDB.SQLExecute("UPDATE sortTable SET extraSortColumnWAD = REPLACE(REPLACE(myColumn" + str(sortColumn) + ", ' ', ''), ',', '') ") 'convert the text to a number
		    If memoryDB.Error Then Return 'not sure why we got an error, but leave the table alone
		    tempSQL = tempSQL + "ORDER BY extraSortColumnWAD " + If(sortAscending, "ASC", "DESC")
		  Else
		    tempSQL = tempSQL + "ORDER BY myColumn" + str(sortColumn) + " " + If(sortAscending, "ASC", "DESC")
		  End If
		  rs = memoryDB.SQLSelect(tempSQL)
		  //If commonWAD.isDatabaseRecordSetErrorWAD(memoryDB, rs, False) Then Return
		  
		  Me.DeleteAllRows
		  while not rs.EOF
		    Me.AddRow ""
		    For tempInt As Integer = 0 to rs.FieldCount - 2 'don't place extraSortColumnWAD into the original WebListBox!
		      Me.Cell(Me.LastIndex, tempInt) = rs.IdxField(tempInt + 1).StringValue
		    Next
		    
		    rs.MoveNext
		  Wend
		  
		  If Me.RowCount > 0 Then Me.ListIndex = 0 'highlight the first row
		  
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="AlternateRowColor"
			Visible=true
			Group="Behavior"
			InitialValue="&cEDF3FE"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ColumnCount"
			Visible=true
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ColumnWidths"
			Visible=true
			Group="Behavior"
			InitialValue="*"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasHeading"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="200"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Visible=true
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinimumRowHeight"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Multiline"
			Visible=true
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PrimaryRowColor"
			Visible=true
			Group="Behavior"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="200"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
