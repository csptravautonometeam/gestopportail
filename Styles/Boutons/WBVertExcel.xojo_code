#tag WebStyle
WebStyle WBVertExcel
Inherits WebStyle
	#tag WebStyleStateGroup
		border-top=1px solid 008040FF
		border-left=1px solid 008040FF
		border-bottom=1px solid 008040FF
		border-right=1px solid 008040FF
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
		text-font=System
		text-decoration=false false false false false
		text-size=12px
		misc-background=solid EBEBEBFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		border-top=1px solid EF5B34FF
		border-left=1px solid EF5B34FF
		border-bottom=1px solid EF5B34FF
		border-right=1px solid EF5B34FF
		misc-background=solid E0292CFF
		text-color=505050FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle WBVertExcel
#tag EndWebStyle

