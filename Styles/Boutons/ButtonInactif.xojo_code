#tag WebStyle
WebStyle ButtonInactif
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid 808080FF
		text-color=FAFAFAFF
		border-top=1pt solid ADD0E5FF
		border-left=1pt solid ADD0E5FF
		border-bottom=1pt solid ADD0E5FF
		border-right=1pt solid ADD0E5FF
		corner-topleft=10px
		corner-bottomleft=10px
		corner-bottomright=10px
		corner-topright=10px
		text-align=center
		text-size=12px
		text-font=System
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle ButtonInactif
#tag EndWebStyle

