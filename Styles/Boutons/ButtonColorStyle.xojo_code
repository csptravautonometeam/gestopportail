#tag WebStyle
WebStyle ButtonColorStyle
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid EF5B34FF
		text-color=FAFAFAFF
		border-top=1px solid ADD0E5FF
		border-left=1px solid ADD0E5FF
		border-bottom=1px solid ADD0E5FF
		border-right=1px solid ADD0E5FF
		corner-topleft=10px
		corner-bottomleft=10px
		corner-bottomright=10px
		corner-topright=10px
		text-align=center
		text-size=12px
		text-font=System
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		border-top=1px solid 000000FF
		border-left=1px solid 000000FF
		border-bottom=1px solid 000000FF
		border-right=1px solid 000000FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle ButtonColorStyle
#tag EndWebStyle

