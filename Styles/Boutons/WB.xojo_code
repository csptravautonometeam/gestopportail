#tag WebStyle
WebStyle WB
Inherits WebStyle
	#tag WebStyleStateGroup
		border-top=1px solid 16ACDDFF
		border-left=1px solid 16ACDDFF
		border-bottom=1px solid 16ACDDFF
		border-right=1px solid 16ACDDFF
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
		text-font=System
		text-decoration=false false false false false
		text-size=12px
		misc-background=solid E6E6E6FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		border-top=1px solid EF5B34FF
		border-left=1px solid EF5B34FF
		border-bottom=1px solid EF5B34FF
		border-right=1px solid EF5B34FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle WB
#tag EndWebStyle

