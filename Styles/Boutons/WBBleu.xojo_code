#tag WebStyle
WebStyle WBBleu
Inherits WebStyle
	#tag WebStyleStateGroup
		border-top=1px solid FFFFFFFF
		border-left=1px solid FFFFFFFF
		border-bottom=1px solid FFFFFFFF
		border-right=1px solid FFFFFFFF
		corner-topleft=10px
		corner-bottomleft=10px
		corner-bottomright=10px
		corner-topright=10px
		text-font=System
		text-decoration=false false false false false
		text-size=12px
		misc-background=solid 16ACDDFF
		text-color=FFFFFFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		border-top=1px solid EF5B34FF
		border-left=1px solid EF5B34FF
		border-bottom=1px solid EF5B34FF
		border-right=1px solid EF5B34FF
		text-decoration=True false false false false
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle WBBleu
#tag EndWebStyle

