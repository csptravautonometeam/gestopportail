#tag WebStyle
WebStyle ButtonNormal
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid EBEBEBFF
		text-size=12px
		border-top=1pt solid 16ACDDFF
		border-left=1pt solid 16ACDDFF
		border-bottom=1pt solid 16ACDDFF
		border-right=1pt solid 16ACDDFF
		corner-topleft=15pt
		corner-bottomleft=15pt
		corner-bottomright=15pt
		corner-topright=15pt
		text-align=center
		text-font=System,Lucida Grande,Verdana,Arial,sans-serif
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		border-top=1px solid EF5B34FF
		border-left=1px solid EF5B34FF
		border-bottom=1px solid EF5B34FF
		border-right=1px solid EF5B34FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle ButtonNormal
#tag EndWebStyle

