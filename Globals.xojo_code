#tag Module
Protected Module Globals
	#tag Method, Flags = &h0
		Function GMTextConversion(tempString As String) As String
		  Dim c as TextConverter
		  c=GetTextConverter(GetTextEncoding(&h500), GetTextEncoding(0))
		  return c.convert( tempString)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SafeSQL(TempString As String) As String
		  Dim goodString As String
		  goodString = ReplaceAll(TempString, "'", "''")
		  Return goodString
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub selectListItem(targetList As WebListBox, itemID As Integer, itemWithID As Boolean)
		  If itemWithID Then
		    For i As Integer = 0 To targetList.RowCount -1
		      If targetList.RowTag(i).IntegerValue = itemID Then
		        targetList.Selected(i) = True
		        Exit For i
		      End If
		    Next i
		  Else
		    If targetList.RowCount > 0 Then
		      If itemID <= targetList.RowCount -1 And itemID >-1 Then
		        targetList.Selected(itemID) = True
		      Else
		        targetList.Selected(0) = True
		      End If
		    Else
		      Exit Sub
		    End If
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		dialogText As String
	#tag EndProperty

	#tag Property, Flags = &h0
		modalArray() As WebContainer
	#tag EndProperty

	#tag Property, Flags = &h0
		showInactifs As Boolean = False
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="dialogText"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="showInactifs"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
